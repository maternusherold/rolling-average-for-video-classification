# Rolling average on single frames for video classification

**Synopsis:** trying to do simple video classification using rolling average on 
single image classification; inspired by this [pyimagesearch.com](https://www.pyimagesearch.com/2019/07/15/video-classification-with-keras-and-deep-learning) post

Despite checking out such an simple (& interesting) approach to do video
 classification 
this project is also to experience various options when training a model. This 
includes e.g. Tensorflow's data API to build a pipeline.


## Approach
incorporate image analysis on each video frame combined with an rolling average 
of the last *k* frames to do video classification - kind of sliding window.

Steps:
  1. loop over all frames $`f_i`$ in video, $`\quad f \in F`$
  2. apply image analysis on frame $`f_i`$ 
  3. compute average `predicted_sport` of last $`k`$ predictions 


## Results
A trained model on the categories `[]` can be found in the repository.
To run the model on a video use the script in `model/video_classification.py` 
or see the example below to inspect its capabilities. 

#### Fitting process
The model was fit on sports images to predict on single 
$`224 \times 224 \times 3`$ images. 
The model is based on a ImageNet-pretrained MobileNetV2.
 
TODO: plot of learning curves <br>
TODO: final accuracy and loss 

#### Classifying a video
![sample soccer video](output/soccer.mp4)

[Video source](https://www.videvo.net/video/men-playing-soccer-in-street-3/5570/)


## Remarks
There was further effort put into analysing the framework's generalization 
abilities or the like. 
