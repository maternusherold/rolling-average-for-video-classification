#!/usr/env/bin python

import numpy as np
import pathlib
import tensorflow as tf
from utils import augment_images, print_info, save_object

from sklearn.preprocessing import OneHotEncoder
from typing import List, Tuple


class DataPipeline:

    def __init__(self, data_root: str = './data/images', 
                 path_patterns: List[str] = ['/*.png', '/*.jpg'],
                 sport_types: List[str] = ['football', 'basketball'],
                 batch_size: int = 32, prefetch: int = 4):
        self.data_root = pathlib.Path(data_root)
        self.path_patterns = path_patterns
        self.sport_types = sport_types
        self.batch_size = batch_size
        self.prefetch = prefetch
        self.auto_tune = tf.data.experimental.AUTOTUNE
        self.image_paths = None
        self.image_labels = None
        self.dataset_size = None
        self.dataset_categories = None
        self.training_index = None
        self.validation_index = None
        self.testing_index = None
        self.dataset = None

    @staticmethod
    def encode_labels(image_labels) -> np.array:
        """One-hot encode labels.

        :return: encoded data as numpy array
        """
        encoder = OneHotEncoder()
        # reshape to have label per row
        image_labels = np.array(image_labels).reshape(-1, 1)
        encoded_data = encoder.fit_transform(image_labels).toarray()

        # save enocoder
        save_object(object_to_save=encoder, path='./label_encoder.pickle')
        return encoded_data

    @staticmethod
    def preprocess_image(image):
        """TODO: docstring

        :return:
        """
        image = tf.image.decode_png(image, channels=3)
        image = tf.image.resize(image, [224, 224])
        image = image / 255.
        return image

    def grep_paths_per_directory(self, sport_type: str):
        """TODO: docstring

        :param sport_type:
        :return:
        """
        paths = list()

        # use different file patterns to search for
        for pattern in self.path_patterns:
            # search for images in subdirectory and given pattern
            paths_pattern = self.data_root.glob(sport_type + pattern)
            paths += [str(path) for path in paths_pattern]

        return paths

    def grep_paths(self) -> Tuple[List[str], List[str]]:
        """TODO: docstring

        :return:
        """
        paths = list()
        labels = list()
        for sport_type in self.sport_types:
            # search directory for image paths
            paths_in_dir = self.grep_paths_per_directory(sport_type=sport_type)

            # add image paths to list of paths
            paths += paths_in_dir

            # add image labels to list of labels
            labels += len(paths_in_dir) * [sport_type]

        self.image_paths = paths
        self.image_labels = self.encode_labels(labels)
        return paths, labels

    def data_set_status(self):
        """Prints the size of current dataset. """
        # set attributes
        self.dataset_size = len(self.image_paths)
        self.dataset_categories = len(self.image_labels[0])

        print_info(f'Dataset size: \t {self.dataset_size}' + '\n' +
                   f'Number categories: \t {self.dataset_categories}')

    def load_and_preprocess(self, path, label):
        """TODO: docstring

        :return:
        """
        image = tf.read_file(path)
        image = self.preprocess_image(image)
        return image, label

    def build_dataset(self) -> tf.data.Dataset:
        """Constructs TensorFlow dataset. """
        dataset = tf.data.Dataset.from_tensor_slices((self.image_paths,
                                                      self.image_labels))
        dataset = dataset.shuffle(buffer_size=self.dataset_size)
        dataset = dataset.repeat()
        dataset = dataset.map(self.load_and_preprocess,
                              num_parallel_calls=self.auto_tune)
        dataset = dataset.batch(batch_size=self.batch_size)
        dataset = dataset.prefetch(buffer_size=self.prefetch)

        self.dataset = dataset
        return dataset

    def split_dataset(self, train_val_split: List[float] = [.7, .15, .15]):
        """Splitting dataset into training, validation and testing dataset.

        :param train_val_split: list of splits
        :return:
        """
        if np.sum(train_val_split) != 1:
            raise ValueError(f'splits do not add up to 1: '
                             f'{np.sum(train_val_split)} != 1')

        # calculate split indices
        self.training_index = np.ceil(self.dataset_size * train_val_split[0])
        self.validation_index = np.ceil(self.dataset_size * train_val_split[1])
        self.testing_index = np.floor(self.dataset_size * train_val_split[2])

        dataset_training = self.dataset.take(self.training_index)
        # add rotation, flipping etc should only be applied to training images
        dataset_training = dataset_training.map(augment_images,
                                                num_parallel_calls=self.auto_tune)
        dataset_validation = self.dataset.skip(self.training_index)
        dataset_testing = self.dataset.skip(self.training_index +
                                            self.validation_index)

        return dataset_training, dataset_validation, dataset_testing

# # TODO: remove after testing
# if __name__ == '__main__':
#     import os
#     path = 'images'
#     print(os.listdir(path))
#     dp = DataPipeline(data_root='images')
#     dp.grep_paths()
#     dp.data_set_status()
#     print(len(dp.image_paths))
#     dp.build_dataset()
