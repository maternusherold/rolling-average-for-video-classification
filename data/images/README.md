# Training images
sport images from 22 classes in the following structure

```
.
├── badminton
├── baseball
├── basketball
├── boxing
├── chess
├── cleaned.csv
├── cricket
├── fencing
├── football
├── formula1
├── gymnastics
├── hockey
├── ice_hockey
├── kabaddi
├── models
├── motogp
├── shooting
├── swimming
├── table_tennis
├── tennis
├── volleyball
├── weight_lifting
├── wrestling
└── wwe

23 directories, 1 file
```

all images come from [this](https://github.com/anubhavmaity/Sports-Type-Classifier) GitHub repository by Anubhav Maity. however, all files not having the extensions `.jpg` or `.png` were removed.
