import cv2
import numpy as np
import pickle
import tensorflow as tf

from collections import deque
from utils import load_scaler, print_info
from typing import Tuple


class VideoClassifier:

    def __init__(self, path_model: str = './model/model.model',
                 path_video: str = './data/videos/test/soccer.mp4',
                 path_label_encoder: str = './label_encoder.pickle',
                 path_output: str = './output',
                 queue_size: int = 32, frame_size: Tuple[int, int] = (224, 224)):
        self.model = tf.keras.models.load_model(path_model)
        self.video = cv2.VideoCapture(path_video)
        self.video_writer = cv2.VideoWriter_fourcc(*'MJPG')
        self.label_encoder = load_scaler(path_label_encoder)
        self.path_output = path_output
        self.queue = deque(maxlen=queue_size)
        self.frame_size = frame_size
        self.writer = None
        self.frame_width = None
        self.frame_height = None

    @staticmethod
    def show_image(frame_show):
        """TODO: docstring, typing

        :param frame_show:
        :return:
        """
        cv2.imshow('Output image', frame_show)
        return cv2.waitKey(1) & 0xFF

    @staticmethod
    def draw_prediction_on_frame(frame, prediction):
        """TODO: docstring, typing

        :param frame:
        :param prediction:
        :return:
        """
        text = "activity: {}".format(prediction)
        return cv2.putText(frame, text, (35, 50), cv2.FONT_HERSHEY_SIMPLEX,
                           1.25, (0, 255, 0), 5)

    def prepare_frame(self, frame):
        """TODO: docstring, typing

        :param frame:
        :return:
        """
        frame_copy = frame.copy()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        return cv2.resize(frame, self.frame_size).astype('float32'), frame_copy

    def predict_on_frame(self, frame):
        """TODO: docstring, typing

        :param frame:
        :return:
        """
        prediction = self.model.predict(np.expand_dims(frame, axis=0))[0]
        self.queue.append(prediction)
        return prediction

    def sliding_window_averaging(self):
        """TODO: docstring

        :return:
        """
        results_averaged = np.array(self.queue).mean(axis=0)
        sport_index = np.argmax(results_averaged)
        return self.label_encoder.categories_[0][sport_index]

    def write_video_to_output(self, frame_out):
        """TODO: docstring

        :return:
        """
        if self.writer is None:
            self.writer = cv2.VideoWriter(self.path_output, self.video_writer, 30,
                                          (self.frame_width, self.frame_height), True)
        self.writer.write(frame_out)

    def classify_video(self):
        """TODO: docstring

        :return:
        """
        print_info(f'starting video classification')

        while True:
            pointer, frame = self.video.read()

            # end of video if there's no pointer left
            if not pointer:
                break

            # init frame width and height if not done already
            if self.frame_width is None or self.frame_height is None:
                self.frame_height, self.frame_width = frame.shape[:2]

            # prepare frame for prediction
            frame, frame_copy = self.prepare_frame(frame=frame)

            # perform prediction on frame
            self.predict_on_frame(frame=frame)

            # average current predictions
            predicted_sport = self.sliding_window_averaging()
            frame_drawn_pred = self.draw_prediction_on_frame(frame=frame_copy,
                                                             prediction=predicted_sport)

            self.write_video_to_output(frame_out=frame_drawn_pred)
            key = self.show_image(frame_show=frame_drawn_pred)

            # exit on key 'q'
            if key == ord('q'):
                break

        print_info('done classifying')
        print_info(f'cleaning up')
        # clean up
        self.writer.release()
        self.video_writer.release()
        print_info('DONE')


# TODO: remove after testing!
if __name__ == '__main__':

    vc = VideoClassifier(path_model='../mobile_net_sports.model',
                         path_label_encoder='../label_encoder.pickle',
                         path_output='../output/soccer.mp4',
                         path_video='/home/maternus/Coding/'
                                    'rolling-average-for-video-classification/'
                                    'data/videos/example_clips/street_soccer.mp4',
                         queue_size=32)
    vc.classify_video()
