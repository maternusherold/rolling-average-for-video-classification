#!/usr/bin/env python

import numpy as np

from data.data_pipeline import DataPipeline
from model import SportImagePredictor
from utils import print_info, plot_metric

if __name__ == '__main__':
    BATCH_SIZE = 256
    EPOCHS = 15

    print_info(f'Training for {EPOCHS} epochs and a batch size of {BATCH_SIZE}')
    root = '/home/maternusherold/rolling-avg/data/'

    dp = DataPipeline(data_root=root, sport_types=['cricket', 'badminton',
                                                   'football', 'shooting', 'boxing',
                                                   'gymnastics', 'hockey', 'swimming',
                                                   'table_tennis', 'wrestling',
                                                   'tennis', 'baseball', 'volleyball',
                                                   'basketball', 'weight_lifting'],
                      batch_size=BATCH_SIZE, prefetch=2)
    dp.grep_paths()
    dp.data_set_status()
    dp.build_dataset()
    train, val, test = dp.split_dataset()

    model = SportImagePredictor(batch_size=BATCH_SIZE, epochs=EPOCHS,
                                learning_rate=.0001,
                                output_dimensions=15)
    model.build_model()
    model.compile_model()

    spe = int(np.ceil(dp.training_index / dp.batch_size))
    val_steps = int(np.ceil(dp.validation_index / dp.batch_size))

    history = model.fit_model(data_training=train, data_validation=val,
                              steps_per_epoch=spe, steps_per_validation=val_steps)
    plot_metric(history=history, font_color='b')

    model.save_model('./model/mobile_net_sports.model')
