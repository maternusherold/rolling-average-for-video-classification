#!/usr/env/bin python

import tensorflow as tf
import tensorflow.keras.applications as tf_models

from data import data_pipeline
from typing import Tuple
from utils import print_info


class SportImagePredictor:

    def __init__(self, batch_size: int = 32, epochs: int = 10,
                 learning_rate: float = .00001,
                 base_model: tf_models = tf_models.MobileNetV2(
                     input_shape=(224, 224, 3), include_top=False,
                     weights='imagenet'),
                 base_model_trainable_layers: int = 0,
                 output_dimensions: int = 22, verbose: int = 1):
        self.batch_size = batch_size
        self.epochs = epochs
        self.learning_rate = learning_rate
        self.base_model = base_model
        self.trainable_layers = base_model_trainable_layers
        self.output_dimensions = output_dimensions
        self.verbose = verbose
        self.model = None

    def build_model(self, **kwargs):
        """TODO: docstring

        :param kwargs:
        :return:
        """
        # TODO: check for trainable layers - if any or just False
        # ...
        self.base_model.trainable = False

        # TODO: set trainable layers
        # ...

        model = tf.keras.Sequential([
            self.base_model,
            tf.keras.layers.AveragePooling2D(pool_size=(7, 7), name='avg_pool'),
            tf.keras.layers.Flatten(name='flatten'),
            tf.keras.layers.Dense(units=512, activation='relu', name='dense_01'),
            tf.keras.layers.Dense(self.output_dimensions, activation='softmax',
                                  name='dense_02')
        ])
        self.model = model
        return model

    def compile_model(self):
        """TODO: docstring """
        self.model.compile(optimizer=tf.keras.optimizers.RMSprop(lr=self.learning_rate),
                           loss='categorical_crossentropy', metrics=['accuracy'])
        if self.verbose:
            self.model.summary()

    def fit_model(self, data_training: tf.data.Dataset,
                  data_validation: tf.data.Dataset, steps_per_epoch: int = 1,
                  steps_per_validation: int = 1):
        """TODO: docstring

        :return:
        """
        history = self.model.fit(x=data_training, epochs=self.epochs,
                                 verbose=self.verbose,
                                 validation_data=data_validation,
                                 steps_per_epoch=steps_per_epoch,
                                 validation_steps=steps_per_validation)
        return history

    def save_model(self, save_to_path: str):
        """Saves serialized model to disk.

        :param save_to_path: path to save the model to including model name
        """
        # serialize the model to disk
        print_info('serializing network...' + '\n' +
                   f'saving model to {save_to_path}')
        self.model.save(save_to_path)
