#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import tensorflow as tf

from sklearn.base import TransformerMixin
from typing import List


def get_file_extensions(path: str):
    """Returns a list of all file extensions in path.

    :param path: path to search in
    :return: set of file extensions in path  
    """
    f = set()  # list of file extensions

    for _, _, files in os.walk(path):
        for filename in files:
            file_extension = filename.split('.')[-1]
            f.add(file_extension)
    return f


def cut_prefix(word: str, delimiter: chr) -> str:
    """Removes prefix from word

    :param word: string literal where a specific prefix shall be removed
    :param delimiter: prefix to remove
    :return: cleaned string literal
    """
    postfix = word.split(delimiter)[-1]
    if postfix:
        return postfix
    else:
        return word


def count_files(file_names: List[str], pattern: str) -> float:
    """Counts files matching a pattern string in a list of file names

    :param file_names: list of file names to consider
    :param pattern:
    :return: number of occurrences
    """
    count = 0
    for file in file_names:
        if cut_prefix(file, delimiter='.') == pattern:
            count += 1
    return count


def get_total_files(path: str, file_extensions: List[str]) -> dict:
    """Counts total number of files with file_extensions in path

    :param path: path to search
    :param file_extensions: files to consider
    :return: dict of file and number of matching occurrences
    """
    file_extensions = [cut_prefix(word=ext, delimiter='.') for ext in file_extensions]
    ret = dict.fromkeys(file_extensions, 0)

    for _, _, files in os.walk(path):
        for k, v in ret.items():
            ret[k] += count_files(file_names=files, pattern=k)
    return ret


def rotate_img(image: tf.Tensor) -> tf.Tensor:
    """Randomly rotates an image by 0, 90, 180 or 270 degrees.

    :param image: image to rotate
    :return: rotated image as TensorFlow tensor
    """
    return tf.image.rot90(image, tf.random_uniform(shape=[], minval=0,
                                                   maxval=4, dtype=tf.int32))


def flip_img(image: tf.Tensor) -> tf.Tensor:
    """Randomly flips images horizontally and vertically.

    :param image: image to flip
    :return: flipped image as TensorFlow tensor
    """
    image = tf.image.random_flip_left_right(image)
    image = tf.image.random_flip_up_down(image)
    return image


def color_img(image: tf.Tensor) -> tf.Tensor:
    """Adds color augmentation to images.

    :param image: image to color
    :return: colored image as TensorFlow tensor"""
    image = tf.image.random_hue(image, 0.1)
    image = tf.image.random_saturation(image, 0.5, 11.5)
    image = tf.image.random_brightness(image, 0.05)
    image = tf.image.random_contrast(image, 0.5, 1.5)
    return image


def augment_images(image, label):
    """TODO: docstring

    :return:
    """
    image = rotate_img(image)
    image = flip_img(image)
    image = color_img(image)
    return image, label


def plot_metric(history, font_color: str = 'k',
                save_to: str = 'training_results_sports_classification.png'):
    """Plots training and validation accuracy.

    :param history: history returned from tf.keras model
    :param font_color: font color in plot
    :param save_to: location to save the image to incl. file name
    """
    acc = history.history['acc']
    val_acc = history.history['val_acc']

    loss = history.history['loss']
    val_loss = history.history['val_loss']
    x = np.arange(1, len(acc) + 1, 1)

    plt.figure(figsize=(8, 8))

    plt.plot(x, acc, label='Training Accuracy')
    plt.plot(x, val_acc, label='Validation Accuracy')

    plt.plot(x, loss, label='Training Loss')
    plt.plot(x, val_loss, label='Validation Loss')

    plt.legend(loc='lower left')
    plt.ylabel('value', color=font_color)
    plt.ylim([min(plt.ylim()), 1])
    plt.xticks(x, color=font_color)
    plt.yticks(color=font_color)
    plt.title('Training and Validation Accuracy', color=font_color)

    plt.savefig(save_to, dpi=300)
    plt.show()


def print_info(info: str):
    """Prints provided info in a special format.

    :param info: information to print as string
    """
    print(f'[INFO] '.ljust(80, '='))
    print(info)
    print(''.ljust(80, '='))


def save_object(object_to_save, path: str):
    """Saving a given object in pickle format.

    :param object_to_save: serializable object to save
    :param path: path to save to including name
    """
    with open(path, 'wb') as file:
        file.write(pickle.dumps(object_to_save))


def load_scaler(path_to_scaler: str) -> TransformerMixin:
    """Loading TransformerMixin object from pickle.

    :param path_to_scaler:
    :return: scaler like MinMaxScaler
    """
    try:
        with open(path_to_scaler, 'rb') as file:
            return pickle.load(file)
    except:
        print("Unexpected error:", sys.exc_info()[0])
